<?php

class pathManager
{
    /**
     * Parameters - Private
     */
    private $__path;
    private $__entriesIsCache = false;
    private $__entries = array();
    private $__entriesRecursiveIsCache = false;
    private $__entriesRecursive = array();

    /**
     * Methods - Magic
     */
    function __construct( $path ) {
        $this->__path = $path;
    }

    /**
     * Mathod - Static
     */
    static public function isDir( $path ) {
        if ( is_dir( $path ) ) {
            return true;
        }
        return false;
    }
    static public function isFile( $path ) {
        if ( file_exists( $path ) ) {
            return true;
        }
        return false;
    }
    static public function isWritable( $path ) {
        if ( is_writable( $path ) ) {
            return true;
        }
        return false;
    }

    /**
     * Methods - Public
     */
    public function getAllEntries( $recursively = false ) {
        return $this->_cacheEntries( $recursively );
    }
    public function getAllFiles( $recursively = false ) {
        $entries = $this->_cacheEntries( $recursively );
        return $this->_getAllFiles( $entries );
    }
    public function getAllDirectories( $recursively = false ) {
        $entries = $this->_cacheEntries( $recursively );
        return $this->_getAllDirectories( $entries );
    }
    public function getAllFilesType( $extensions, $recursively = false ) {
        if ( !is_array( $extensions ) ) {
            $extensions = array( $extensions );
        }

        $entries = $this->getAllFiles( $recursively );
        $files   = array();
        foreach ($entries as $file) {
            $infos = pathinfo($file);
            if ( isset( $infos['extension'] ) && in_array($infos['extension'], $extensions) ) {
                $files[] = $file;
            }
        }
        return $files;
    }
    public function resetCache( $recursively ) {
        if ( $recursively === true ) {
            $this->__entriesIsCache = false;
            return true;
        }
        $this->__entriesRecursiveIsCache = false;
        return true;
    }
    public function removePath() {
        $this->resetCache( true );
        $entries = $this->getAllEntries( true );

        $this->_removePath( $entries );
        rmdir( $this->__path );
    }

    /**
     * Methods - Protected
     */
    protected function _removePath( $entries, $path = NULL ) {
        if ( $path === NULL ) {
            $path = $this->__path;
        }
        foreach ($entries as $basename => $values) {
            if ( is_array( $values ) ) {
                $newPath = $path . '/' . $basename;
                $this->_removePath( $values, $newPath );
                rmdir($newPath);
                continue ;
            }
            unlink( $values );
        }
    }
    protected function _cacheEntries( $recursively ) {
        if ( $recursively === false ) {
            if ( $this->__entriesIsCache === false ) {
                $this->__entries = $this->_getAllEntries( $this->__path, false );
                $this->__entriesIsCache = true;
            }
            return $this->__entries;
        }
        if ( $this->__entriesRecursiveIsCache === false ) {
            $this->__entriesRecursive = $this->_getAllEntries( $this->__path, true );
            $this->__entriesRecursiveIsCache = true;
        }
        return $this->__entriesRecursive;
    }
    protected function _setPath( $path ) {
        if ( !self::isDir( $path ) ) {
            throw new Exception( $path . " No such directory" , 1);
        }
        $this->__path = $path;
    }
    protected function _getAllEntries( $path, $recursively = false ) {
        $entries = scandir($path);
        $name = basename($path);
        $cleannedEntries = array();

        foreach ($entries as $entry) {
            if ( $entry == "." || $entry == ".." ) {
                continue ;
            }
            $realPathEntry = realpath($path . "/" . $entry);
            if ( is_dir( $realPathEntry ) && $recursively === true ) {
                $cleannedEntries[ $entry ] = $this->_getAllEntries( $realPathEntry, $recursively);
                continue ;
            }
            $cleannedEntries[ $entry ] = $realPathEntry;
        }
        return $cleannedEntries;
    }
    protected function _getAllFiles( $entries ) {
        $files = array();

        foreach ($entries as $path) {
            if ( is_array( $path ) ) {
                $subFiles = $this->_getAllFiles( $path );
                $files = array_merge($files, $subFiles);
            }
            else if ( is_file( $path ) ) {
                $files[] = $path;
                continue;
            }
        }
        return $files;
    }
    protected function _getAllDirectories( $entries, $previousPath = "/" ) {
        $directory = array();
        if ( $previousPath = "/" ) {
            $previousPath = $this->__path . "/";
        }

        foreach ($entries as $dirName => $contain) {
            if ( is_array( $contain ) ) {
                $directory[] = $previousPath . $dirName;
                $path = $previousPath . $dirName . "/";
                $subDirectory = $this->_getAllDirectories( $contain, $path );
                $directory = array_merge($directory, $subDirectory);
            }
            else if ( is_dir( $contain ) ) {
                $directory[] = $previousPath . $dirName;
            }
        }
        return $directory;
    }
}
